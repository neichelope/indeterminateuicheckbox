//
//  CategoryView.swift
//  IndeterminateUICheckbox
//
//  Created by Neil Chester Lopez on 2/21/20.
//  Copyright © 2020 Neil Chester Lopez. All rights reserved.
//

import UIKit

protocol CategoryViewDelegate {
    func didTapCheckbox(for category: Category)
}


class CategoryView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var checkbox: UIButton!
    @IBOutlet weak var line: UIView!
    
    var category: Category!
    var delegate: CategoryViewDelegate?
    
    @IBAction func didTapCheckbox(_ sender: Any) {
        delegate?.didTapCheckbox(for: category)
    }
}
