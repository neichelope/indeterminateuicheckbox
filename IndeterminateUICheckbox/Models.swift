//
//  Item.swift
//  IndeterminateUICheckbox
//
//  Created by Neil Chester Lopez on 2/21/20.
//  Copyright © 2020 Neil Chester Lopez. All rights reserved.
//

struct Item {
    
    var name: String
    var categoryName: String
    var number: Int
    var value: String
    var checkboxState: CheckBoxState = .unselected
}

struct Category {
    var name: String
    var state: CheckBoxState = .unselected
}
