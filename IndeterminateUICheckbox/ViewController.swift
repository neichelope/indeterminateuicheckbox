//
//  ViewController.swift
//  IndeterminateUICheckbox
//
//  Created by Neil Chester Lopez on 2/21/20.
//  Copyright © 2020 Neil Chester Lopez. All rights reserved.
//

import UIKit

enum CheckBoxState {
    case selected
    case unselected
    case indeterminate
}

class ViewController: UIViewController {
    @IBOutlet weak var overallCheckbox: UIButton!
    
    var overallCheckboxState: CheckBoxState = .unselected {
        didSet {
            switch overallCheckboxState {
                
            case .selected:
                overallCheckbox.tintColor = .green
                overallCheckbox.setImage(UIImage(named: "selected"), for: .normal)
            case .unselected:
                overallCheckbox.setImage(UIImage(named: "default"), for: .normal)
                overallCheckbox.tintColor = .white
            case .indeterminate:
                overallCheckbox.tintColor = .orange
                overallCheckbox.setImage(UIImage(named: "indeterminate"), for: .normal)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func didTapOverallCheckbox(_ sender: Any) {
        switch overallCheckboxState {
            
        case .selected:
            overallCheckboxState = .unselected
        case .unselected:
            overallCheckboxState = .selected
        case .indeterminate:
            overallCheckboxState = .unselected
        }
        
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "OverallCheckboxDidTap"), object: nil, userInfo: ["new_state" : overallCheckboxState])

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "Table":
            (segue.destination as! TableViewController).delegate = self
        default:
            break
        }
        
    }
    
}

extension ViewController: TableViewControllerDelegate {
    func tableViewController(updateFor overallCheckboxState: CheckBoxState) {
        self.overallCheckboxState = overallCheckboxState
        
        print("OVERALL UPDATE \(overallCheckboxState)")
    }
    
    
}

