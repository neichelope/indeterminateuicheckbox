//
//  ItemCell.swift
//  IndeterminateUICheckbox
//
//  Created by Neil Chester Lopez on 2/21/20.
//  Copyright © 2020 Neil Chester Lopez. All rights reserved.
//

import UIKit

protocol ItemCellDelegate {
    func didTapCheckbox(for item: Item)
}

class ItemCell: UITableViewCell {
    
    @IBOutlet weak var checkbox: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var upperLine: UIView!
    @IBOutlet weak var lowerLine: UIView!
    
    var item: Item!
    var state: CheckBoxState = .unselected
    var delegate: ItemCellDelegate?
    
    @IBAction func didTapCheckbox(_ sender: Any) {
        delegate?.didTapCheckbox(for: item)
    }
}
