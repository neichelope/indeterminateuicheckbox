//
//  TableViewController.swift
//  IndeterminateUICheckbox
//
//  Created by Neil Chester Lopez on 2/21/20.
//  Copyright © 2020 Neil Chester Lopez. All rights reserved.
//

import UIKit

protocol TableViewControllerDelegate {
    func tableViewController(updateFor overallCheckboxState:CheckBoxState)
}

class TableViewController: UITableViewController {
    
    var delegate: TableViewControllerDelegate?
    
    var categories: [Category]!
    var items:[String:[Item]]!
    
    var isStateUpdateFromOverallCheckbox = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        categories = [Category(name: "PGAY7"),Category(name: "81KRU"),Category(name: "GTW70")]
        
        let items = [Item(name: "item1", categoryName: "GTW70", number: 1, value: "DEF"), Item(name: "item 2", categoryName: "GTW70", number: 2, value: "DEF"), Item(name: "item 3", categoryName: "GTW70", number: 3, value: "DEF")]
        self.items = ["GTW70":items]
        
        let categoryNib = UINib(nibName: "CategoryView", bundle: nil)
        tableView.register(categoryNib, forHeaderFooterViewReuseIdentifier: "header")
        
        let itemNib = UINib(nibName: "ItemView", bundle: nil)
        tableView.register(itemNib, forCellReuseIdentifier: "cell")
        
        tableView.reloadData()
        
        NotificationCenter.default.addObserver(forName: Notification.Name("OverallCheckboxDidTap"), object: nil, queue: nil) { (notification) in
            let newState = notification.userInfo!["new_state"] as! CheckBoxState
            self.isStateUpdateFromOverallCheckbox = true
            guard self.categories != nil else { return }
            for category in self.categories {
                var category = category
                category.state = newState
                var newItems = [Item]()
                if let items = self.items[category.name] {
                    for item in items {
                        var item = item
                        item.checkboxState = newState
                        newItems.append(item)
                    }
                    
                    self.items.updateValue(newItems, forKey: category.name)
                }
                if let index = self.categories.firstIndex(where: ({$0.name == category.name})) {
                    self.categories.remove(at: index)
                    self.categories.insert(category, at: index)
                }
                
                self.tableView.reloadData()
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return categories.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let category = categories[section]
        
        return items[category.name]?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let category =  categories[indexPath.section]
        let items = self.items![category.name]
        let item = items![indexPath.row]
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? ItemCell else { return UITableViewCell() }
        
        cell.item = item
        switch item.checkboxState {
            
        case .selected:
            cell.checkbox.tintColor = .green
            cell.checkbox.setImage(UIImage(named: "selected"), for: .normal)
        case .unselected:
            cell.checkbox.setImage(UIImage(named: "default"), for: .normal)
            cell.checkbox.tintColor = .white
        case .indeterminate:
            break
        }
        cell.numberLabel.text = "\(item.number)"
        cell.titleLabel.text = item.name
        cell.valueLabel.text = item.value
        cell.lowerLine.isHidden = (items!.count - 1) == indexPath.row
        
        cell.delegate = self
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let categoryView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as! CategoryView
        let category = categories[section]
        categoryView.category = category
        categoryView.categoryLabel.text = category.name
        categoryView.line.isHidden = items[category.name]?.isEmpty ?? true
        categoryView.delegate = self
        
        switch category.state {
            
        case .selected:
            categoryView.checkbox.tintColor = .green
            categoryView.checkbox.setImage(UIImage(named: "selected"), for: .normal)
        case .unselected:
            categoryView.checkbox.setImage(UIImage(named: "default"), for: .normal)
            categoryView.checkbox.tintColor = .white
        case .indeterminate:
            categoryView.checkbox.tintColor = .orange
             categoryView.checkbox.setImage(UIImage(named: "indeterminate"), for: .normal)
        }

        
        return categoryView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
    func updateOverallCheckbox() {
        var isNotAllCategorySelected: Bool!
        var isNoneCategorySelected = true

        for category in self.categories {
            if isNotAllCategorySelected == nil {
                isNotAllCategorySelected = category.state == .selected
            } else {
                isNotAllCategorySelected = category.state == .selected && isNotAllCategorySelected
            }
            
            if isNoneCategorySelected {
                isNoneCategorySelected =  category.state == .unselected
            }
        }
        
        var overAllCheckboxNewState: CheckBoxState
        if !isNoneCategorySelected && !(isNotAllCategorySelected ?? false ){
            overAllCheckboxNewState = .indeterminate
        } else if !isNotAllCategorySelected {
            overAllCheckboxNewState = .unselected
        } else {
            overAllCheckboxNewState = .selected
        }
        
        delegate?.tableViewController(updateFor: overAllCheckboxNewState)
    }
}

extension TableViewController: CategoryViewDelegate {
    func didTapCheckbox(for category: Category) {
        var category = category
        
        switch category.state {
            
        case .selected:
            category.state = .unselected
        case .unselected:
            category.state = .selected
        case .indeterminate:
            category.state = .unselected
        }
        
        guard let index = categories.firstIndex(where: ({ $0.name == category.name})) else { return }
        categories.remove(at: index)
        categories.insert(category, at: index)
        
        tableView.reloadData()
        
        if let items = self.items?[category.name] {
            var newItems = [Item]()
            for item in items {
                var newItem = item
                newItem.checkboxState = category.state
                newItems.append(newItem)
            }
            self.items.removeValue(forKey: category.name)
            self.items.updateValue(newItems, forKey: category.name)
            self.tableView.reloadData()
        }
        
        updateOverallCheckbox()
    }
    
    
}

extension TableViewController: ItemCellDelegate {
    
    func didTapCheckbox(for item: Item) {
        var item = item
        
        switch item.checkboxState {
        case .selected:
            item.checkboxState = .unselected
        case .unselected:
            item.checkboxState = .selected
        case .indeterminate:
            break
        }
        
        guard let index = items[item.categoryName]?.firstIndex(where: ({$0.number == item.number})) else { return }
        items[item.categoryName]!.remove(at: index)
        items[item.categoryName]!.insert(item, at: index)
        
        let categories = items.keys
        
        for categoryName in categories {
            var isNotAllSelected: Bool!
            var isNoneSelected = true
            
            if let categoryItems = items[categoryName] {
                for item in categoryItems {
                    
                    if isNotAllSelected == nil {
                        isNotAllSelected = item.checkboxState == .selected
                    } else {
                        isNotAllSelected = item.checkboxState == .selected && isNotAllSelected
                    }
                    
                    if isNoneSelected {
                        isNoneSelected = item.checkboxState == .unselected
                    }
                }
            }
            var category: Category
            if !isNoneSelected && !(isNotAllSelected ?? false ){
                category = Category(name: categoryName, state: .indeterminate)
                
            } else if !isNotAllSelected {
                category = Category(name: categoryName, state: .unselected)
            } else {
                category = Category(name: categoryName, state: .selected)
            }
            
            guard let index = self.categories.firstIndex(where: ({$0.name == categoryName})) else { return }
            self.categories.remove(at: index)
            self.categories.insert(category, at: index)
            tableView.reloadData()
        }
        
        updateOverallCheckbox()

    }
    
}
